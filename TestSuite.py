
__author__ = 'Atiur Siddique'
__email__ = "A.Siddique@bom.gov.au"

import unittest
from TestProcessStatus import TestProcessStatus
from TestProcess import TestProcess
from TestProcessManager import TestProcessManager
from TestProcessUsingQueue import TestProcessUsingQueue
from TestEndToEnd import TestEndToEnd
import sys


"""
Runs all test from the designated UnitTest classes.
"""

if __name__ == "__main__":
    # set verbosity level
    verbosity = 2
    # list all testclasses to run unittests from
    test_class_list = [TestProcessStatus, TestProcess, TestProcessManager, TestEndToEnd]

    if len(sys.argv) == 2 and sys.argv[1] == "with_queue":
        test_class_list.append(TestProcessUsingQueue)

    for test_class in test_class_list:
        suite = unittest.TestLoader().loadTestsFromTestCase(test_class)
        unittest.TextTestRunner(verbosity=verbosity).run(suite)
