""" This module offers Process class as a stateless, core class to manage
processes. Process status is closely tied to ProcessStatus class.
"""

__author__ = 'Atiur Siddique'
__email__ = "A.Siddique@bom.gov.au"

import subprocess
import os


class Process:
    """
    Process module that abstracts subprocess library functions for easy use
    with a process status object.

    Assumes the following are set
    logger = None
    """

    def __init__(self):
        """
        Set the logger by name.

        Args:
            None.

        Returns:
            None.

        Raises:
            None.
        """

    def schedule(self, processStatus):
        """
        Schedule the job to run. The default strategy is to run
        instantaneously in a separate thread.

        Args:
            processStatus: ProcessStatus that has the command to run.

        Returns:
            None.

        Raises:
            Exception: if status is None.
        """
        self.logger.info("Scheduling job {} to run right now using threading.".format(
            processStatus.processRecord.name))
        # in a threaded environment we don't need to schedule separately,
        # it is done for the logical sequence. And then we can jump into
        # running the process.
        processStatus.scheduled()
        self.launch(processStatus)

    def replace_environment_variables(self, str):
        if str.startswith('$'):
            token = str[1:]
            if token in os.environ.keys():
                return os.environ[token]
            self.logger.error("Environment variable '{}' is not defined in environment".format(token))
        return str

    def launch(self, processStatus):
        """Launch the process from status.

        Args:
            status: ProcessStatus that has the command to run.

        Returns:
            None.

        Raises:
            Exception: if status is None.
        """
        self.logger.info("Launching process '{}'.".format(processStatus.processRecord.name))

        # find tokens in command
        command_tokens = processStatus.processRecord.command.split(' ')
        # strip off blank whitespaces and replace environment variables
        command_tokens = [self.replace_environment_variables(token.strip()) for token in command_tokens]
        # create the process
        self.logger.debug("Running command: '{}'".format(" ".join(command_tokens)))
        processStatus.pipe = subprocess.Popen(command_tokens)
        self.logger.debug("Pipe opened for process '{}' and pid '{}'.".format(
            processStatus.processRecord.name,
            processStatus.pipe.pid))
        processStatus.started(processStatus.pipe.pid)

    def kill(self, processStatus):
        """Launch the process from status.

        Args:
            status: process status that has the command to run.

        Returns:
            None.

        Raises:
            Exception: if status is None.
        """
        if not processStatus.executionRecord or processStatus.executionRecord.status != 'Started':
            raise Exception("Process status is None or not 'Started'.")

        if not self.exists(processStatus):
            self.logger.debug("Process '{}' does not exist.".format(
                processStatus.name))
            return

        self.logger.warning("Killing process '{}'".format(processStatus.processRecord.name))
        processStatus.pipe.kill()
        if processStatus.pipe.poll():  # has it been killed?
            returnCode = processStatus.pipe.wait()
            if returnCode:
                processStatus.killed(returnCode)
            else:
                processStatus.killed()
        else:
            self.terminate(processStatus)

    def terminate(self, processStatus):
        """
        Terminate the running process from status.

        Args:
            status: ProcessStatus that has process pid to kill.

        Returns:
            None.

        Raises:
            Exception: if process has not started yet.
        """
        if not processStatus.executionRecord or processStatus.executionRecord.status != 'Started':
            raise Exception("Process status is None or not 'Started'.")

        if not self.exists(processStatus):
            self.logger.debug("Process '{}' does not exist.".format(
                processStatus.processRecord.name))
            return

        self.logger.warning("Killing process '{}' by force.".format(
            processStatus.processRecord.name))
        processStatus.pipe.terminate()
        if processStatus.pipe.poll():  # has it been killed?
            returnCode = processStatus.pipe.wait()
            if returnCode:
                processStatus.killed(returnCode)
            else:
                processStatus.killed()

    def waitToFinish(self, processStatus):
        """
        Wait for the process to finish and return exit code.

        Args:
            status: ProcessStatus that has the reference to the process pipe.

        Returns:
            None.

        Raises:
            Exception: if process has finished already or has not started yet.
        """
        self.logger.info("Waiting for process '{}' to finish.".format(
            processStatus.processRecord.name))
        returnValue = processStatus.pipe.wait()
        self.logger.info("Process '{}' finished with return value {}.".format(
            processStatus.processRecord.name, returnValue))
        processStatus.finished(returnValue)

    def exists(self, processStatus):
        """
        Checks if the process is still running

        Args:
            status: ProcessStatus that has the reference to the process pipe.

        Returns:
            Boolean.
            True, if the process is still running.
            False, other wise.

        Raises:
            None.
        """
        if os.path.isfile('/proc/{}/stat'.format(processStatus.executionRecord.pid)) and not processStatus.pipe.poll():
            # still running
            self.logger.debug("Process {} is still running".format(processStatus.processRecord.name))
            return True
        # finished or killed
        self.logger.debug("Process {} is no longer running".format(processStatus.processRecord.name))
        return False
