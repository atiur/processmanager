"""This module test the ProcessUsingQueue module.
"""

__author__ = 'Atiur Siddique'
__email__ = "A.Siddique@bom.gov.au"

import unittest
from springpython.context import ApplicationContext
from springpython.config import XMLConfig
import time
import os
import re


class TestProcessUsingQueue(unittest.TestCase):
    """Tests ProcessUsingQueue class

    Sets the following:
    process
    logger
    context
    databaseTemplate
    processStatus

    """

    contextFile = 'test-pm-context-with-queue.xml'
    processStatusTestTouch05_file = 'test05'

    def setUp(self):
        config = XMLConfig(self.contextFile)
        self.context = ApplicationContext(config)
        self.logger = self.context.get_object("logger")
        self.logger.init()
        self.databaseTemplate = self.context.get_object("databaseTemplate")
        self.processStatus = self.context.get_object("processStatus")
        self.processStatus.findProcess('DummySilentGrep')
        self.process = self.context.get_object("process")
        self.longSleepingProcessStatus = self.context.get_object("processStatus")
        self.longSleepingProcessStatus.findProcess('Sleep100Seconds')

    def tearDown(self):
        self.processStatus.clean()
        try:
            self.process.kill(self.processStatus)
            self.process.kill(longSleepingProcessStatus)
        except Exception:
            pass
        self.processStatus = None
        self.process = None
        regex = re.compile('STD.*\.[o,e][0-9]*')
        filelist = [f for f in os.listdir(".") if regex.match(f)]
        for file in filelist:
            os.remove(file)

    def test_schedule_should_schedule_job_in_queue(self):
        self.process.schedule(self.processStatus)

    def test_launch_should_raise_with_queue(self):
        self.assertRaises(Exception, self.process.waitToFinish, self.processStatus)

    def test_exists_and_kill(self):
        self.assertRaises(Exception, self.process.exists, self.longSleepingProcessStatus)
        self.process.schedule(self.longSleepingProcessStatus)
        self.assertTrue(self.process.exists(self.longSleepingProcessStatus))
        self.process.kill(self.longSleepingProcessStatus)
        time.sleep(1)
        self.assertFalse(self.process.exists(self.longSleepingProcessStatus))

    def test_terminate_should_raise_with_queue(self):
        self.assertRaises(Exception, self.process.terminate, self.processStatus)

    def test_waitToFinish_should_raise_with_queue(self):
        self.assertRaises(Exception, self.process.waitToFinish, self.processStatus)

    def test_replace_environment_variables_in_process_command(self):
        if os.path.exists(self.processStatusTestTouch05_file):
            os.remove(self.processStatusTestTouch05_file)
        self.processStatusTestTouch05 = self.context.get_object("processStatus")
        self.processStatusTestTouch05.findProcess('TestTouch05')
        self.assertFalse(os.path.exists(self.processStatusTestTouch05_file))
        self.process.schedule(self.processStatusTestTouch05)
        print "Waiting for process with qid {}".format(self.processStatusTestTouch05.executionRecord.qid)
        while True:
            if self.process.exists(self.processStatusTestTouch05):
                time.sleep(10)  # wait for 10 secords before checking again
            else:
                 break
        self.assertTrue(os.path.exists(self.processStatusTestTouch05_file))
        if os.path.exists(self.processStatusTestTouch05_file):
            os.remove(self.processStatusTestTouch05_file)
            pass

    def runTest(self):
        pass

if __name__ == "__main__":
    testProcessStatus = TestProcessUsingQueue()
    unittest.main(verbosity=2)
