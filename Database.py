""" This module offers all database functionalities
More specifically, it offers CloseDBConnectionWrappingInterceptor that can commit or close
connections after each matched query execution.
"""

__author__ = 'Atiur Siddique'
author_email = 'A.Siddique@bom.gov.au',

from springpython.aop import MethodInterceptor


# Commit and possibly close transactions after each database method with
#  a transaction. This is facilitated through a MethodInterceptor proxy object.
class CloseDBConnectionWrappingInterceptor(MethodInterceptor):
    """Assumes the following are set
    self.logger is initialized to standard Logger object
    self.commitSession is unset or set to "True"
    self.closeConnection is unset or set to "True"
    """
    def invoke(self, invocation):
        """ Let the original method proceed and then commit the ongoing
         transaction.
         @param invocation original function being invoked through this proxy.
         @retval result result returned from invocation target.
         """

        self.logger.debug("Proceeding with invocation.")
        result = invocation.proceed()

        if self.commitSession:
            """ We should commit the transaction after invoked function finishes.
            Otherwise, every other transaction begin request will wait for
            the first one to finish.
            """
            self.logger.debug(
                "Invocation finished, committing ongoing transaction.")
            invocation.instance.connection_factory.commit()
        else:
            self.logger.debug("Transaction commit disabled.")

        if self.closeConnection:
            """ We do not need to close the connection, since sqlite3db and mysqldb
            are thread safe.
            """
            self.logger.debug("Closing current database connection.")
            invocation.instance.databaseTemplate.connection_factory.close()
        else:
            self.logger.debug("Connection closing disabled.")

        return result
