# Provides a springpython initializable interface to standard python logging

__author__ = 'Atiur Siddique'
author_email = 'A.Siddique@bom.gov.au',

import logging


# class logger that interfaces standard python logger
class Logger:
    # Assumes the following to be initialized
    # application_name = "ProcessManager"
    # log_format = "#+++|%(asctime)s|%(name)s.%(module)s.%(funcName)s()(line %(lineno)d)|%(levelname)s|%(message)s"
    # date_format = "%Y%m%dT%H%M%SZ"
    # log_level_name = "ERROR"

    # Initializes system wide logger
    # logger = None

    # maps string loglevel to standard levels
    def map_strloglevel_to_loglevel(self, log_level_name):
        return {
            "CRITICAL": logging.CRITICAL,
            "FATAL": logging.FATAL,
            "ERROR": logging.ERROR,
            "WARNING": logging.WARNING,
            "INFO": logging.INFO,
            "DEBUG": logging.DEBUG,
            "NOTSET": logging.NOTSET
        }[log_level_name]

    # Configures logging
    def init(self):
        logging.basicConfig(format=self.log_format, datefmt=self.date_format, level=self.map_strloglevel_to_loglevel(self.log_level_name))
        self.logger = logging.getLogger(self.application_name)

    # The following functions delegate all responsibility to system logger.
    # I did not subclass Logger or LoggerAdapter to make it look like standard logger and initializable by springpython.
    def setLevel(self, level):
        self.logger.setLevel(level)

    def debug(self, msg, *args, **kwargs):
        self.logger.debug(msg, *args, **kwargs)

    def info(self, msg, *args, **kwargs):
        self.logger.info(msg, *args, **kwargs)

    def warning(self, msg, *args, **kwargs):
        self.logger.warning(msg, *args, **kwargs)

    def warn(self, msg, *args, **kwargs):
        self.logger.warning(msg, *args, **kwargs)

    def error(self, msg, *args, **kwargs):
        self.logger.error(msg, *args, **kwargs)

    def exception(self, msg, *args):
        self.logger.exception(msg, *args)

    def critical(self, msg, *args, **kwargs):
        self.logger.critical(msg, *args, **kwargs)

    def log(self, level, msg, *args, **kwargs):
        self.logger.log(level, msg, *args, **kwargs)


class StringContainer(str):

    def __init__(self, source_string):
        self.string = str(source_string)

    def __str__(self):
        return self.string
