""" This module offers ProcessUsingQueue which manages processes
using qsub and qdel tools to work with oracle grid engine OGE."""

__author__ = 'Atiur Siddique'
__email__ = "A.Siddique@bom.gov.au"

import subprocess
from Process import Process
import random
import datetime
import os


class ProcessUsingQueue(Process):
    """
    Process module that abstracts qsub and qdel scripts
    for ease of use with a process status object.

    Assumes the following are set:
    processManagerRunnerName = 'ProcessManagerRunner.py'
    processManagerRunCommand = 'launch'
    qsubCommand = 'qsub'
    qdelCommand = 'qdel'
    qstatCommand = 'qstat'
    awkCommand = 'awk'

    Initializes the following:
    processRecord = None
    execuctionRecord = None
    """

    def __init__(self):
        """
        Initializes ProcessUsingQueue.
        Args:
            None.

        Returns:
            None.

        Raises:
            None.
        """
        pass



    def replace_environment_variables(self, str):
        if str.startswith('$'):
            token = str[1:]
            if token in os.environ.keys():
                #the qsub command takes a C shell script where environment variables must be set using setenv
                return "setenv {} {}".format(token, os.environ[token])
            self.logger.error("Environment variable '{}' is not defined in environment".format(token))
        return ""


    def send_qsub_command(self, process_status, qsub_pipe):
        # find tokens in command
        enviornment_tokens = process_status.processRecord.command.split(' ')
        # strip off blank whitespaces and replace environment variables
        enviornment_tokens = [self.replace_environment_variables(token.strip()) for token in enviornment_tokens]
        # create the process
        command_tokens = process_status.processRecord.command
        c_shell_script = "\n".join(self.qsub_command_prefix) + "\n" + \
            "\n".join(enviornment_tokens) + "\n" + \
             process_status.processRecord.command + "\n" + \
             "\n".join(self.qsub_command_postfix) 
        self.logger.debug("Scheduling job with: {}".format(c_shell_script))
        job_id = qsub_pipe.communicate(input=c_shell_script)[0]
        return job_id.strip()

    def schedule(self, processStatus):
        """
        Schedule the job in queue.

        Args:
            processStatus: ProcessStatus that has the command to run.

        Returns:
            None.

        Raises:
            None
        """
        processStatus.scheduled(0)
        qsubPipe = subprocess.Popen(
            [self.qsubCommand, ],
            stdout=subprocess.PIPE,
            stdin=subprocess.PIPE,
            stderr=subprocess.STDOUT)
        qid = self.send_qsub_command(processStatus, qsubPipe)
        processStatus.updateQid(qid)
        self.logger.info("Scheduled job: {} with qid: {} and executionId: {} "
                         + "at the Queue.".format(processStatus.processRecord.name,
                                                  processStatus.executionRecord.qid,
                                                  processStatus.executionRecord.id))

    def launch(self, status):
        """Launch the process from status.

        Args:
            status: ProcessStatus that has the command to run.

        Returns:
            None.

        Raises:
            Exception: if status is None.
        """
        raise Exception("launch() is incompatible with queue because queue "
                        + "launches a process by itself.")

    def kill(self, processStatus):
        """Launch the process from status.

        Args:
            status: process status that has the command to run.

        Returns:
            None.

        Raises:
            Exception: if status is None.
        """
        self.logger.warn(
            "Invoking qdel command: '%s' with qid: %s for process %s"
            % (self.qdelCommand, processStatus.executionRecord.qid,
               processStatus.processRecord.name))
        subprocess.Popen([self.qdelCommand, processStatus.executionRecord.qid])

    def terminate(self, processStatus):
        """
        Terminate the running process from qeueue.

        Args:
            status: ProcessStatus that has process pid to kill.

        Returns:
            None.

        Raises:
            Exception: if process has not started yet.
        """
        self.logger.warn("Delegating terminate() to kill().")
        self.kill(processStatus)

    def waitToFinish(self, status):
        """
        Wait for the process to finish and return exit code.

        Args:
            status: ProcessStatus that has the reference to the process pipe.

        Returns:
            None.

        Raises:
            Exception: if process has finished already or has not started yet.
        """
        raise Exception("waitToFinish is incompatible with queue.")

    def exists(self, processStatus):
        """
        Check status of the process.

        Args:
            status: ProcessStatus that has the process pipe.

        Returns:
            None.

        Raises:
            Exception: if process has not started yet.
        """

        qstatPipe = subprocess.Popen([self.qstatCommand, ],
                                     stdout=subprocess.PIPE)
        grepPipe = subprocess.Popen(['grep', "^{}".format(processStatus.executionRecord.qid)],
                                    stdin=qstatPipe.stdout,
                                    stdout=subprocess.PIPE)
        qstatPipe.stdout.close()
        wcPipe = subprocess.Popen(['wc', "-l"], stdin=grepPipe.stdout,
                                  stdout=subprocess.PIPE)
        grepPipe.stdout.close()
        numProcess = int(wcPipe.communicate()[0].split()[0].strip())
        wcPipe.stdout.close()
        if numProcess == 1:
            return True

        return False
