__author__ = 'Atiur Siddique'
__email__ = "A.Siddique@bom.gov.au"

from springpython.context import ApplicationContext
from springpython.config import XMLConfig
import subprocess
import os

if __name__ == "__main__":
    config = XMLConfig("pm-context.xml")
    context = ApplicationContext(config)
    db = context.get_object("databaseTemplate")

    f = open("DependencyGraph.gv", "w+")
    f.write("digraph G {")
    # f.write("digraph G {node [shape=box,style=filled,color=\".7 .3 1.0\"];")

    # processes without dependencies can be started after start
    for dependency in db.query_for_list("SELECT name FROM Process where id NOT IN (SELECT process FROM Dependency)"):
        f.write("Start -> \"{}\" [color=green];\n".format(dependency[0]))

    # for each dependency, name2/dependsOn runs before name1/process from dependency table
    for dependency in db.query_for_list("SELECT name1, name2 FROM DependencyNames"):
        # print dependency[0], " -> ", dependency[1], ";"
        f.write("\"{}\" -> \"{}\" [color=red];\n".format(dependency[1], dependency[0]))

    # f.write("node [shape=box,style=filled,color=\".7 .3 1.0\"];")
    # processes that are not depended on, can lead to end
    for dependency in db.query_for_list("SELECT name FROM Process where id NOT IN (SELECT dependsOn FROM Dependency)"):
        # print dependency[0], " -> ", dependency[1], ";"
        f.write("\"{}\" -> Finish [color=blue];\n".format(dependency[0]))

    # processes not in dependency or dependent, should lead to end
#    for dependency in databaseTemplate.query_for_list("SELECT name FROM Process where id not in (SELECT process from
#  " + "Dependency union select dependsOn from Dependency)"):
#        f.write("\"{}\" -> end;\n".format(dependency[0]))

    # processes with
#    for dependency in databaseTemplate.query_for_list("SELECT name FROM Process where id in (SELECT process from
# Dependency WHERE process" +" NOT IN (SELECT dependsOn FROM Dependency))"):
#        f.write("\"{}\" -> end;\n".format(dependency[0]))

    # processes
#    for dependency in databaseTemplate.query_for_list("SELECT name FROM Process where id in (SELECT dependsOn FROM
#  Dependency WHERE dependsOn NOT IN" +" (SELECT process from Dependency))"):
#        f.write("start -> \"{}\";\n".format(dependency[0]))

#    for dependency in databaseTemplate.query_for_list("SELECT name FROM Process"):
#        print "start -> ", dependency[0], ";"
#        #f.write("start -> \"{}\";\n".format(dependency[0]))
#        print dependency[0], " -> end;"
#        #f.write("\"{}\" -> end;\n".format(dependency[0]))

    f.write("}")
    f.close()

    subprocess.Popen(['dot', '-Tpdf', 'DependencyGraph.gv', '-o', 'DependencyGraph.pdf']).wait()
    os.remove("DependencyGraph.gv")
