__author__ = 'Atiur Siddique'
__email__ = "A.Siddique@bom.gov.au"

from springpython.context import ApplicationContext
from springpython.config import XMLConfig
import unittest


class TestProcess(unittest.TestCase):
    """Test Process class.

    Sets the following
    process = None
    logger = None
    context = None
    databaseTemplate = None
    processStatus = None
    """
    contextFile = 'test-pm-context.xml'

    def setUp(self):
        config = XMLConfig(self.contextFile)
        self.context = ApplicationContext(config)
        self.logger = self.context.get_object("logger")
        self.logger.init()
        self.databaseTemplate = self.context.get_object("databaseTemplate")
        self.processStatus = self.context.get_object("processStatus")
        self.processStatus.findProcess('DummySilentGrep')
        self.process = self.context.get_object("process")
        self.longSleepingProcessStatus = self.context.get_object("processStatus")
        self.longSleepingProcessStatus.findProcess('Sleep100Seconds')

    def tearDown(self):
        self.processStatus.clean()
        self.processStatus = None
        self.process = None

    def test_launch(self):
        """launch() should launch the process that switches its status to "Started"
        """
        self.process.schedule(self.processStatus)
        self.assertEquals(self.processStatus.executionRecord.status, "Started")

    def test_killShouldThrowOnNoneProcess(self):
        """ Trying to kill a None process should raise Exception
        """
        self.assertRaises(Exception, self.process.kill, self.processStatus)

    def test_killShouldNotThrowAfterLaunchingProcess(self):
        """ A launched process should be killed and not throw when kill is called
        """
        self.process.schedule(self.longSleepingProcessStatus)
        self.process.kill(self.longSleepingProcessStatus)

    def test_terminateShouldThrowOnNoneProcess(self):
        """ Trying to terminate a None process should raise Exception
        """
        self.assertRaises(Exception, self.process.terminate, self.processStatus)

    def test_terminateShouldNotThrowAfterLaunchingProcess(self):
        """ A launched process should be terminated and not throw when terminate is called
        """
        self.process.schedule(self.longSleepingProcessStatus)
        self.process.terminate(self.longSleepingProcessStatus)

    def test_waitToFinishshouldThrowOnNoneProcess(self):
        """ waitToFinish() should raise Exception on None process
        """
        self.assertRaises(Exception, self.process.waitToFinish, self.processStatus)

    def test_waitToFinishshouldNotThrowAfterLaunchingProcess(self):
        """ waitToFinish() should wait until a launched process has finished and not throw
        """
        self.process.schedule(self.processStatus)
        self.process.waitToFinish(self.processStatus)

    def test_replace_environment_variables(self):
        """ fname should be replaced by 'test05' that is defined in test.sh
        """
        self.assertEquals(self.process.replace_environment_variables('$fname'), 'test05')
