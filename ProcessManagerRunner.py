__author__ = 'Atiur Siddique'
__email__ = "A.Siddique@bom.gov.au"

from ProcessManager import ProcessManager
import sys

"""
This script runs an instance of ProcessManager with designated context.
"""

if __name__ == "__main__":
    processManager = ProcessManager()

    if len(sys.argv) < 1:
        exit(0)
    elif len(sys.argv) == 1:
        processManager.run()
    elif len(sys.argv) == 2:
        if sys.argv[1] == "status":
            processManager.printAllProcessStatus()
        elif sys.argv[1] == "pause":
            processManager.disableAllProcessing()
            processManager.waitUntilAllRunningProcessesFinish()
        elif sys.argv[1] == "resume":
            processManager.enableAllProcessing()
            processManager.run()
        elif sys.argv[1] == "kill":
            processManager.killAllProcesses()
