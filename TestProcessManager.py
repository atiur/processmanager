__author__ = 'Atiur Siddique'
__email__ = "A.Siddique@bom.gov.au"

import unittest
import os
import re
from ProcessManager import ProcessManager
from mock import MagicMock


class TestProcessManager(unittest.TestCase):

    testFile = 'TestFile.txt'
    testContextName = 'test-pm-context.xml'

    def setUp(self):
        self.processManager = ProcessManager(self.testContextName)
        self.databaseTemplate = self.processManager.context.get_object("databaseTemplate")
        self.silentGrepProcessStatus = self.processManager.context.get_object("processStatus")
        self.silentGrepProcessStatus.findProcess("DummySilentGrep")
        self.processManager.secondsBetweenRetries = 0
        if os.path.isfile(self.testFile):
            os.remove(self.testFile)

    def tearDown(self):
        if os.path.isfile(self.testFile):
            os.remove(self.testFile)
        # To clean up, get any process status object and call its clean().
        self.silentGrepProcessStatus.clean()
        # for file in os.listdir("."):
        #    if re.search(".+\.sh", file):
        #        os.remove(os.path.join('.', file))

    def test_findAllRunnableProcess(self):
        """ findAllRunnableProcess() should return process known to exist
        """
        foundRunnableProcess = False
        for process in self.processManager.findAllRunnableProcess():
            if process.name == 'DummySilentGrep':
                foundRunnableProcess = True
        if not foundRunnableProcess:
            self.fail()

    def test_runNextRunnableProcess(self):
        """ findNextRunnableProcess() should return known next process
        """
        self.assertEquals(self.processManager.findNextRunnableProcess().name,
                          'DummyLsCommand')

    def test_hasPendingProcess(self):
        """ hasPendingProcess() should return True on test database with pending processes
        """
        self.assertTrue(self.processManager.hasPendingProcess())

    def test_updateAllProcessStatus(self):
        """ updateAllProcessStatus() should call exists() on known processes
        """
        self.processManager.processStatusList.append(self.silentGrepProcessStatus)
        self.processManager.process.schedule(self.silentGrepProcessStatus)
        self.silentGrepProcessStatus.killed = MagicMock(name='killed')
        self.processManager.process.exists = MagicMock(name='exists')
        self.processManager.process.exists.return_value = False
        self.processManager.updateAllProcessStatus()
        self.processManager.process.exists.assert_called_once_with(
            self.silentGrepProcessStatus)

    def test_killAllProcesses(self):
        """ killAllProcesses() should kill scheduled and launched process
        """
        processStatus = self.silentGrepProcessStatus
        self.processManager.processStatusList.append(processStatus)
        self.processManager.process.schedule(processStatus)
        self.processManager.process.kill = MagicMock(name='kill')
        self.processManager.process.exists = MagicMock(name='exists')
        self.processManager.process.exists.return_value = True
        self.processManager.killAllProcesses()
        self.processManager.process.kill.assert_called_once_with(
            processStatus)

    def test_run(self):
        """ run() should call update all and run all
        """
        self.processManager.updateAllProcessStatus = MagicMock(
            name='updateAllProcessStatus')
        self.processManager.runAllRunnableProcess = MagicMock(
            name='runAllRunnableProcess')
        self.processManager.run(1)
        self.processManager.updateAllProcessStatus.assert_called_once_with()
        self.processManager.runAllRunnableProcess.assert_called_once_with()
