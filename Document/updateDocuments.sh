#!/bin/sh
doxygen Doxygen.conf
for type in html latex ; do
    if [ -d $type ] ; then
        tar -cf - $type | gzip -c --best > $type.tgz
        rm -rf $type
    fi
done
