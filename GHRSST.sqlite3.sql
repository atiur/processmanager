
-- Table: Process
CREATE TABLE Process ( 
    id              INTEGER         PRIMARY KEY ASC AUTOINCREMENT
                                    NOT NULL,
    name            VARCHAR( 50 ),
    command         VARCHAR( 100 ),
    enabled         INT             NOT NULL
                                    DEFAULT '1',
    restartRequired INT             NOT NULL
                                    DEFAULT '0' 
);

INSERT INTO [Process] ([id], [name], [command], [enabled], [restartRequired]) VALUES (1, 'S00-Ancillary_WARNCONTINUE', 'getAncillaryFiles -B -M -G -I -N -A <date>', 1, 0);
INSERT INTO [Process] ([id], [name], [command], [enabled], [restartRequired]) VALUES (2, 'S10-15aProcessDayPass_BATCH', 'processDayPass  -3U -f -p -m <date> 15a', 1, 0);
INSERT INTO [Process] ([id], [name], [command], [enabled], [restartRequired]) VALUES (3, 'S10-15dProcessDayPass_BATCH', 'processDayPass  -3U -f -p -m <date> 15d', 1, 0);
INSERT INTO [Process] ([id], [name], [command], [enabled], [restartRequired]) VALUES (4, 'S10-16aProcessDayPass_BATCH', 'processDayPass  -3U -f -p -m <date> 16a', 1, 0);
INSERT INTO [Process] ([id], [name], [command], [enabled], [restartRequired]) VALUES (5, 'S10-16dProcessDayPass_BATCH', 'processDayPass  -3U -f -p -m <date> 16d', 1, 0);
INSERT INTO [Process] ([id], [name], [command], [enabled], [restartRequired]) VALUES (6, 'S10-18aProcessDayPass_BATCH', 'processDayPass  -3U -f -p -m <date> 18a', 1, 0);
INSERT INTO [Process] ([id], [name], [command], [enabled], [restartRequired]) VALUES (7, 'S10-18dProcessDayPass_BATCH', 'processDayPass  -3U -f -p -m <date> 18d', 1, 0);
INSERT INTO [Process] ([id], [name], [command], [enabled], [restartRequired]) VALUES (8, 'S10-19aProcessDayPass_BATCH', 'processDayPass  -3U -f -p -m <date> 19a', 1, 0);
INSERT INTO [Process] ([id], [name], [command], [enabled], [restartRequired]) VALUES (9, 'S10-19dProcessDayPass_BATCH', 'processDayPass  -3U -f -p -m <date> 19d', 1, 0);
INSERT INTO [Process] ([id], [name], [command], [enabled], [restartRequired]) VALUES (10, 'S21-18ProcessCompositeC1_BATCH', 'processDayComposite -w -f -p -C -s 18 <date> "day night" 1', 1, 0);
INSERT INTO [Process] ([id], [name], [command], [enabled], [restartRequired]) VALUES (11, 'S21-19ProcessCompositeC1_BATCH', 'processDayComposite -w -f -p -C -s 19 <date> "day night" 1', 1, 0);
INSERT INTO [Process] ([id], [name], [command], [enabled], [restartRequired]) VALUES (12, 'DummyLsCommand', 'ls', 1, 0);

-- Table: Configuration
CREATE TABLE Configuration ( 
    variable VARCHAR PRIMARY KEY
                     NOT NULL,
    value    VARCHAR 
);

INSERT INTO [Configuration] ([variable], [value]) VALUES ('<date>', 20130313);

-- Table: Dependency
/*
 * Foreign keys
 */
CREATE TABLE Dependency ( 
    id        INTEGER PRIMARY KEY AUTOINCREMENT
                      NOT NULL,
    process   INTEGER,
    dependsOn INTEGER,
    FOREIGN KEY ( process ) REFERENCES Process ( id ) ON DELETE NO ACTION
                                                          ON UPDATE NO ACTION,
    FOREIGN KEY ( dependsOn ) REFERENCES Process ( id ) ON DELETE NO ACTION
                                                            ON UPDATE NO ACTION 
);

INSERT INTO [Dependency] ([id], [process], [dependsOn]) VALUES (1, 2, 1);
INSERT INTO [Dependency] ([id], [process], [dependsOn]) VALUES (2, 3, 1);
INSERT INTO [Dependency] ([id], [process], [dependsOn]) VALUES (3, 1, 7);

-- Table: Execution
/*
 * Foreign keys
 */
CREATE TABLE Execution ( 
    id               INTEGER  PRIMARY KEY
                              NOT NULL,
    process          INTEGER  NOT NULL,
    started          DATETIME DEFAULT CURRENT_TIMESTAMP,
    pid              INT,
    status           VARCHAR  NOT NULL,
    returnValue      INT,
    finishedOrKilled DATETIME,
    qid              INT      DEFAULT 0,
    FOREIGN KEY ( process ) REFERENCES Process ( id ) ON DELETE NO ACTION
                                                          ON UPDATE NO ACTION 
);

INSERT INTO [Execution] ([id], [process], [started], [pid], [status], [returnValue], [finishedOrKilled], [qid]) VALUES (6, 6, '2013-11-20 02:40:03', 0, 'Started', null, null, 0);
INSERT INTO [Execution] ([id], [process], [started], [pid], [status], [returnValue], [finishedOrKilled], [qid]) VALUES (8, 3, '2013-11-20 02:40:03', 0, 'Finished', 0, '2013-11-20 02:40:03', 0);

-- Index: Configuration_idx_variable01
CREATE UNIQUE INDEX Configuration_idx_variable01 ON Configuration ( 
    variable 
);


-- View: PendingProcess
CREATE VIEW PendingProcess AS
       SELECT *
         FROM Process p
        WHERE id NOT IN ( 
                  SELECT process
                    FROM Execution e 
              );
;


-- View: RunnablePendingProcess
CREATE VIEW RunnablePendingProcess AS
       SELECT *
         FROM PendingProcess
        WHERE id NOT IN ( 
                  SELECT process
                    FROM Dependency d
                   WHERE dependsOn NOT IN ( 
                             SELECT process
                               FROM Execution e1 
                         ) 
                          
              );
;


-- View: ExecutedProcess
CREATE VIEW ExecutedProcess AS
       SELECT *
         FROM Process
        WHERE id IN ( 
                  SELECT process
                    FROM Execution 
              );
;


-- View: DependencyNames
CREATE VIEW DependencyNames AS
       SELECT p1.name AS name1,
              p1.id,
              d.process,
              d.dependsOn,
              p2.name AS name2,
              p2.id
         FROM Process p1, 
              Dependency d, 
              Process p2
        WHERE p1.id = d.process 
              AND
              d.dependsOn = p2.id;
;


-- View: ProcessWithDate
CREATE VIEW ProcessWithDate AS
       SELECT id,
              name,
              REPLACE( command, c.variable, c.value ),
              enabled,
              restartRequired
         FROM Process, 
              Configuration c
        WHERE c.variable = '<date>';
;

