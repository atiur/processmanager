"""
This module offers all classes used in ORM and represents database rows
as objects.
"""

__author__ = 'Atiur Siddique'
__email__ = "A.Siddique@bom.gov.au"


from springpython.database.core import RowMapper
from springpython.database.transaction import transactional


class DatabaseRecord:
    """Represents one database record
    sets the following
    databaseTemplate = None
    """

    @transactional(["PROPAGATION_REQUIRED"])
    def updateDatabase(self):
        """ updates database to reflect current status of this object.
        Args:
            None.

        Returns:
            None.

        Raises:
            Exception: on database transaction problems.
        """
        raise Exception("ProcessManagerRowMapper.updateDatabase() not"
                        + " defined, it is supposed to be sub-classed before use.")


class DatabaseRecordMapper(RowMapper):
    """Maps one database record to an object.
    Assumes the following are set
    databaseTemplate = None
    """
    pass


class ProcessRecord(DatabaseRecord):
    """
    Represents one database row from Process table.
    """

    def __init__(self, tid, name, command, enabled, restartRequired):
        self.id = tid
        self.name = name
        self.command = command
        self.enabled = enabled
        self.restartRequired = restartRequired

    @transactional(["PROPAGATION_REQUIRED"])
    def updateDatabase(self):
        """
        Update database with current properties that may have changed since
        last update.

        Args:
            None.

        Returns:
            None.

        Raises:
            Exception: on database transaction problems.
        """
        self.databaseTemplate.update(
            "UPDATE Process SET name=%s, command=%s,"
            + " enabled=%s, restartRequired=%s WHERE id=%s",
            (self.name, self.command, self.enabled, self.restartRequired,
             self.id))


class ProcessMapper(DatabaseRecordMapper):
    """
    Maps one database row from Process table into Process class.
    """
    def map_row(self, row, metadata=None):
        """ Maps one database result row to one DatabaseRecord object
        """
        record = ProcessRecord(
            tid=row[0], name=row[1], command=row[2], enabled=row[3], restartRequired=row[4])
        record.databaseTemplate = self.databaseTemplate
        return record


class ConfigurationRecord(DatabaseRecord):
    """
    Represents one database row from Configuration table.
    """

    def __init__(self, variable, value):
        self.variable = variable
        self.value = value

    @transactional(["PROPAGATION_REQUIRED"])
    def updateDatabase(self):
        """
        Update database with current properties that may have
        changed since last update.

        Args:
            None.

        Returns:
            None.

        Raises:
            Exception: on database transaction problems.
        """
        self.databaseTemplate.update(
            "UPDATE Configuration SET value=%s where variable=%s",
            (self.value, self.variable))


"""Handles only one row of Configuration table."""


class ConfigurationMapper(DatabaseRecordMapper):
    """
    Maps one database row from Configuration table into Configuration class.
    """

    def map_row(self, row, metadata=None):
        record = ConfigurationRecord(variable=row[0], value=row[1])
        record.databaseTemplate = self.databaseTemplate
        return record


class DependencyRecord(DatabaseRecord):
    """
    Represents one database row from Dependency table.
    """

    def __init__(self, id, process, dependsOn):
        self.id = id
        self.process = process
        self.dependsOn = dependsOn

    @transactional(["PROPAGATION_REQUIRED"])
    def updateDatabase(self):
        """
        Update database with current properties that may have changed
        since last update.

        Args:
            None.

        Returns:
            None.

        Raises:
            Exception: on database transaction problems.
        """
        self.databaseTemplate.update("UPDATE Dependency SET process=%s, "
                                     + "dependsOn=%s WHERE id=%s",
                                     (self.process, self.dependsOn, self.id))


"""Handles only one row of Dependency table."""


class DependencyMapper(DatabaseRecordMapper):
    """
    Maps one database row from Dependency table into Dependency class.
    """

    def map_row(self, row, metadata=None):
        record = DependencyRecord(
            id=row[0], process=row[1], dependsOn=row[2])
        record.databaseTemplate = self.databaseTemplate
        return record


class ExecutionRecord(DatabaseRecord):
    """
    Represents one database row from Execution table.
    """

    def __init__(self, id, process, started, pid, status, returnValue,
                 finishedOrKilled, qid):
        self.id = id
        self.process = process
        self.started = started
        self.pid = pid
        self.status = status
        self.returnValue = returnValue
        self.finishedOrKilled = finishedOrKilled
        self.qid = qid

    @transactional(["PROPAGATION_REQUIRED"])
    def updateDatabase(self):
        """
        Update database with current properties that may have changed
        since last update.

        Args:
            None.

        Returns:
            None.

        Raises:
            Exception: on database transaction problems.
        """
        self.databaseTemplate.update(
            "UPDATE Execution SET process=%s, started=%s, pid=%s, "
            + "status=%s, returnValue=%s, finishedOrKilled=datetime(),"
            + " qid=%s WHERE id=?", (
                self.process, self.started, self.pid, self.status,
                self.returnValue, self.qid, self.id))


"""Handles only one row of Execution table."""


class ExecutionMapper(DatabaseRecordMapper):
    """
    Maps one database row from Execution table into Execution class.
    """

    def map_row(self, row, metadata=None):
        record = ExecutionRecord(
            id=row[0], process=row[1], started=row[2], pid=row[3], status=row[4], returnValue=row[5],
            finishedOrKilled=row[6], qid=row[7])
        record.databaseTemplate = self.databaseTemplate
        return record
