#!/bin/sh
source ~/.bashrc
rm -f .coverage coverage.xml flake8.log pep8.log pylint.log

# setting fname to test05 is a part of testing environment variable in TestEndToEnd.py
export fname=test05
python TestSuite.py
coverage run TestSuite.py
coverage xml
pylint --msg-template='{msg_id}:{line:3d},{column}: {obj}: {msg}'  --max-line-length=160  *.py > pylint.log
pep8 --max-line-length=160 *.py > pep8.log
flake8 --max-complexity 12 --max-line-length=160  --format=pylint *.py > flake8.log
exit 0
