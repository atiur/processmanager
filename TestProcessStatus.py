
__author__ = 'asiddiqu'

from springpython.context import ApplicationContext
from springpython.config import XMLConfig
import unittest


class TestProcessStatus(unittest.TestCase):
    """
    Test ProcessStatus class.

    Sets the following
    logger = None
    context = None
    databaseTemplate = None
    processStatus = None
    """
    contextFile = 'test-pm-context.xml'

    def setUp(self):
        config = XMLConfig(self.contextFile)
        self.context = ApplicationContext(config)
        self.logger = self.context.get_object("logger")
        self.logger.init()
        self.databaseTemplate = self.context.get_object("databaseTemplate")
        self.processStatus = self.context.get_object("processStatus")
        self.processStatus.findProcess('DummyLsCommand')

    def tearDown(self):
        self.processStatus.clean()

    def test_started_shouldBeNoneInTheBeginning(self):
        """ ProcessStatus should not have execution record before being scheduled
        """
        self.assertFalse(hasattr(self.processStatus, 'executionRecord'))

    def test_started_shouldBeStartedAfterStarted(self):
        """ Status should be 'Started' after scheduled() and started() are called
        """
        self.processStatus.scheduled()
        self.processStatus.started(0)
        self.assertTrue(self.processStatus.executionRecord.status == "Started")

    def test_started_shouldNotBeStartedAfterFinished(self):
        """ Status should not be 'Started' after  finished() is called
        """
        self.processStatus.scheduled()
        self.processStatus.started(0)
        self.processStatus.finished(0)
        self.assertTrue(self.processStatus.executionRecord.status != "Started")

    def test_finished_shouldBeFinishedAfterFinished(self):
        """ ProcessStatus should be 'Finished' after finished() is called
        """
        self.processStatus.scheduled()
        self.processStatus.started(0)
        self.processStatus.finished(0)
        self.assertTrue(self.processStatus.executionRecord.status == "Finished")

    def test_finished_shouldNotBeFinishedAfterKilled(self):
        """ Status should not be 'Finished' if killed() was called
        """
        self.processStatus.scheduled()
        self.processStatus.started(0)
        self.processStatus.killed()
        self.assertTrue(self.processStatus.executionRecord.status != "Finished")

    def test_finished_shouldRaiseIfNotStarted(self):
        """
        """
        self.processStatus.scheduled()
        self.assertRaisesRegexp(Exception, "Process status is not 'Started'.",
                                self.processStatus.finished, 0)

    def test_killed_shouldRaiseIfNotStarted(self):
        self.processStatus.scheduled()
        self.assertRaisesRegexp(Exception, "Process status is not 'Started', rather, 'Scheduled'.",
                                self.processStatus.killed)

    def test_killed_shouldBeKilledAfterKilled(self):
        self.processStatus.scheduled()
        self.processStatus.started(0)
        self.processStatus.killed()
        self.assertEquals(self.processStatus.executionRecord.status, "Killed")

    def test_killed_shouldRaiseAfterFinished(self):
        self.processStatus.scheduled()
        self.processStatus.started(0)
        self.processStatus.finished(0)
        self.assertRaisesRegexp(Exception, "Process status is not 'Started'.",
                                self.processStatus.killed)

    def test_statusStartedAndFinished(self):
        self.processStatus.scheduled()
        self.processStatus.started(0)
        self.assertEquals("Started", self.processStatus.executionRecord.status)
        self.processStatus.finished(0)
        self.assertEquals("Finished", self.processStatus.executionRecord.status)

    def test_statusStartedAndKilled(self):
        self.processStatus.scheduled()
        self.processStatus.started(0)
        self.assertEquals("Started", self.processStatus.executionRecord.status)
        self.processStatus.killed()
        self.assertEquals("Killed", self.processStatus.executionRecord.status)

    def test_returnValue(self):
        self.processStatus.scheduled()
        self.processStatus.started(0)
        self.processStatus.finished(0)
        self.assertEquals(0, self.processStatus.executionRecord.returnValue)


if __name__ == "__main__":
    unittest.main(verbosity=2)
