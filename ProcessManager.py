"""
This module offers Process Manager that manages all Process
and ProcessStatus objects to maintain correct order of
launching maintaining intended dependency.
"""

__author__ = 'Atiur Siddique'
__email__ = "A.Siddique@bom.gov.au"

from springpython.context import ApplicationContext
from springpython.config import XMLConfig
import logging
import time
import os
import atexit


class ProcessManager:
    """
    Manage processes run from a single python vm with a single context.


    Sets the following.
    logger = None
    context = None
    process = None
    processMapper = None
    databaseTemplate = None
    processStatusList = []  # Array of all ProcessStatus initiated during this run of processManager.
    """

    # the following are defaults
    contextFile = "pm-context.xml"
    secondsBetweenRetries = 1

    def __init__(self, contextFile=None):
        """
        Initialize ProcessManager, load context file, set logger.

        Args:
            contextFile: name of context XML file.
            logger: pre-configured Logger object.

        Returns:
            None.

        Raises:
            Exception: if contextFile does not exist.
        """
        if not contextFile:
            contextFile = self.contextFile
        self.startUp(contextFile)
        self.logger.debug("Context loaded.")
        # self.logger.debug("Registering killAllProcesses to run at exit.")
        # atexit.register(self.killAllProcesses)

    def startUp(self, contextFile):
        """
        Load context file, load beans, configure and load
        processStatusFactory, setup databases and process singleton.

        Args:
            contextFile: name of context XML file.

        Returns:
            None.

        Raises:
            Exception: if contextFile does not exist.
        """
        if not os.path.isfile(contextFile):
            self.logger.warning("Context file {} does not exist, exiting...".format(
                contextFile))
            exit(0)
        config = XMLConfig(contextFile)
        self.context = ApplicationContext(config)
        self.logger = self.context.get_object("logger")
        self.logger.init()
        self.process = self.context.get_object("process")
        self.databaseTemplate = self.context.get_object("databaseTemplate")
        self.processMapper = self.context.get_object("processMapper")
        self.processStatusList = []

    def findAllRunnableProcess(self, limit=9999):
        """
        Find the next process that does not have any unmet dependency
        and can be run safely.

        Args:
            None.

        Returns:
            None.

        Raises:
            Exception: if contextFile does not exist.
        """
        processes = self.databaseTemplate.query(
            "SELECT * FROM RunnablePendingProcess;".format(limit), None,
            self.processMapper)
        self.logger.info("Found {} number of runnable processes".format(
            len(processes)))
        return processes

    def findNextRunnableProcess(self):
        """
        Find the next process that does not have any dependency which have
        not run and can be run safely.

        Args:
            None.

        Returns:
            None.

        Raises:
            Exception: if contextFile does not exist.
        """
        try:
            process = self.findAllRunnableProcess(1)[0]
        except IndexError:
            self.logger.debug("Found no runnable process.")
            return None
        self.logger.info(
            "Found runnable process name: %s, id: %d, command: %s" %
            (process.name, process.id, process.command))
        return process

    def runNextRunnableProcess(self):
        """Find the next process without unmet dependency and run it.

        Args:
            None.

        Returns:
            None.

        Raises:
            Exception: if contextFile does not exist.
        """
        if not self.isEnabled():
            self.logger.warn("Processing is disabled.")
            return False

        process = self.findNextRunnableProcess()
        if not process:
            self.logger.info("No process is ready to run.")
            return False
        self.logger.debug("Creating process status for {}.".format(
            process.name))
        processStatus = self.context.get_object("processStatus")
        processStatus.findProcess(process.name)
        self.processStatusList.append(processStatus)
        self.logger.debug("Scheduling process {}.".format(
            process.name))
        self.process.schedule(processStatus)
        return True

    def runAllRunnableProcess(self):
        """Find and run all processes that do not have unmet dependency
        and schedule them all at once.

        Args:
            None.

        Returns:
            None.

        Raises:
            None.
        """
        self.logger.debug("Looking for a process to run.")
        while self.runNextRunnableProcess():
            self.logger.debug("Looking for a process to run.")

    def hasPendingProcess(self):
        """
        Return true if there are pending processes in this chain,
        false other wise.

        Args:
            None.

        Returns:
            Boolean.

        Raises:
            Exception: if contextFile does not exist.
        """

        if self.databaseTemplate.query_for_int("SELECT COUNT(*) AS cnt FROM PendingProcess") > 0:
            return True
        return False

    def updateAllProcessStatus(self):
        """
        Find out the status of each reportedly running processes in
        processStatus list.

        Args:
            None.

        Returns:
            None.

        Raises:
            Exception: if contextFile does not exist.
        """
        # check each process status object and determine if it had been killed.
        self.logger.debug("Updating all launched process status.")
        for processStatus in self.processStatusList:
            if processStatus.executionRecord.status == 'Started' and not self.process.exists(processStatus):
                returnCode = processStatus.pipe.wait()
                if returnCode == -9:  # killed
                    processStatus.killed(returnCode)
                else:  # exited by itself
                    processStatus.finished(returnCode)

    def killAllProcesses(self):
        """
        Kill all running processes. This is scheduled to run when the program
        exits.

        Args:
            None.

        Returns:
            None.

        Raises:
            Exception: if contextFile does not exist.
        """
        self.logger.warn("Killing all processes.")
        for processStatus in self.processStatusList:
            try:
                self.logger.warn("Attempting to kill process '{}' with pid {}.".format(processStatus.processRecord.name, processStatus.executionRecord.pid))
                self.process.kill(processStatus)
            except Exception:
                self.logger.warn("Could not kill process %s, probably died already." % processStatus.processRecord.name)

    def singleRun(self):
        """Update all processes and run all runnable processes

        Args:
            None.
        Returns:
            None.
        Raises:
            None.
        """
        self.updateAllProcessStatus()
        self.runAllRunnableProcess()

    def run(self, numberOfIterations=None):
        """
        Run all processes in this chain.

        Args:
            numberOfIterations: how many times to iterate through running all
            runnable processes. Goes for ever, if it is none and tries to run
            until every process has run at least once. Specially useful
            in tests.

        Returns:
            None.

        Raises:
            Exception: if contextFile does not exist.
        """
        if not self.isEnabled():
            self.logger.warn("Processing is disabled.")
            return 0

        self.logger.debug("Process manager started running.")

        if numberOfIterations:
            # for a number of times
            for iteration in range(int(numberOfIterations)):
                self.singleRun()
        else:
            # Run for eternity
            while self.hasPendingProcess():
                self.singleRun()
                time.sleep(self.secondsBetweenRetries)

    def hasRunningProcess(self):
        self.updateAllProcessStatus()
        for processStatus in self.processStatusList:
            if processStatus.status == 'Started':
                return True
        return False

    def waitUntilAllRunningProcessesFinish(self):
        while self.hasRunningProcess():
            time.sleep(self.secondsBetweenRetries)

    def printAllProcessStatus(self):
        """
        Print the whole process dependency list and execution history.

        Args:
            None.

        Returns:
            None.

        Raises:
            Exception: if contextFile does not exist.
        """
        print "Process:"
        for process in self.databaseTemplate.query(
                "SELECT * FROM Process", None,
                self.context.get_object("processMapper")):
            self.logger.debug("Found process {}.".format(process.name))
            print "Process:", process.id, process.name, " '"+process.command+"'"

            print "   -> Dependency list:"
            for dep in self.databaseTemplate.query(
                    "SELECT * FROM Dependency "
                    + "WHERE process = %s", (process.id,),
                    self.context.get_object("dependencyMapper")):
                self.logger.debug("Found dependency {}.".format(dep.id))
                print "   -> ", dep.id, dep.process, dep.dependsOn

            print "   => Execution history:"
            for execution in self.databaseTemplate.query(
                    "SELECT * FROM Execution WHERE process = %s",
                    (process.id,), self.context.get_object("executionMapper")):
                self.logger.debug("Found execution {}.".format(execution.id))
                print "   => ", execution.id, execution.process, \
                    execution.started, execution.pid, execution.status, \
                    execution.returnValue, execution.finishedOrKilled

        print "Configuration variables:"
        for configuration in self.databaseTemplate.query(
                "SELECT * FROM Configuration", (), self.context.get_object("configurationMapper")):
            self.logger.debug("Found configuration {}.".format(configuration.variable))
            print " -> ", configuration.variable, configuration.value

    def disableAllProcessing(self):
        if self.isEnabled():
            self.databaseTemplate.execute("INSERT INTO Configuration(variable, value) VALUES ('Enabled', 'False');")
        self.logger.warn("All processing disabled.")

    def enableAllProcessing(self):
        if self.isEnabled():
            return
        self.databaseTemplate.execute("DELETE FROM Configuration WHERE variable = 'Enabled' AND value = 'False';")
        self.logger.info("All processing enabled.")

    def isEnabled(self):
        processingDisabled = self.databaseTemplate.query_for_int("SELECT count(*) as cnt FROM Configuration WHERE variable = 'Enabled' AND value = 'False';")
        if processingDisabled == 0:
            self.logger.debug("Processing is enabled.")
            return True
        self.logger.debug("Processing is disabled.")
        return False
