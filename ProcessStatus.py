"""This module offers ProcessStatus class to manage process and execution status.
"""

__author__ = 'Atiur Siddique'
__email__ = "A.Siddique@bom.gov.au"

from springpython.database.transaction import transactional


class ProcessStatus:
    """
    Record process status information and update database accordingly.

    Assumes the following are set.
    logger
    processMapper
    executionMapper

    Sets the following.
    processRecord = None
    executionRecord = None

    If the process is run using threading (as opposed to OGE queue) then the following is also set.
    pipe
    """

    def __init__(self, name=None):
        """
        Initialize a process.

        Args:
            None.

        Returns:
            None.

        Raises:
            None.
        """

        if name:
            self.name = name
            self.findProcess(name)

    @transactional(["PROPAGATION_REQUIRED"])
    def findProcess(self, name):
        """
        Find the process by name and record its details in
        current process object.

        Args:
            name: name of the process.

        Returns:
            None.

        Raises:
            Exception if databaseTemplate or logger are not set.
        """

        if not name:
            raise Exception("Process name is None.")

        try:
            self.logger.debug("Looking for Process by name '{}'.".format(
                name))
            self.processRecord = self.databaseTemplate.query(
                "SELECT * FROM ProcessWithDate WHERE name = %s", (name,),
                self.processMapper)[0]
        except IndexError:
            raise Exception("Process name %s not found in database." % (
                name,))

        self.logger.debug("Found process with id: %s, command: %s." % (
            self.processRecord.id, self.processRecord.command))

    @transactional(["PROPAGATION_REQUIRED"])
    def scheduled(self, qid=0):
        """
        Mark the process as scheduled and record its queue id,
        a qid of 0 means it will be scheduled soon and the
        actual qid will be updated later.

        Args:
            qid: queue id, 0 means currently unknown.

        Returns:
            None.

        Raises:
            Exception if processStatus is already set.
        """

        if hasattr(self, 'executionRecord'):
            raise Exception("Execution already exists.")

        self.logger.debug("Marking process {} as 'Scheduled' in database."
                          .format(self.processRecord.name))
        executionId = self.databaseTemplate.insert_and_return_id(
            "INSERT INTO Execution (process, qid, status) VALUES (%s, %s, %s)",
            (self.processRecord.id, qid, 'Scheduled'))
        self.executionRecord = self.databaseTemplate.query(
            "SELECT * FROM Execution WHERE id = %s", (executionId,),
            self.executionMapper)[0]

    @transactional(["PROPAGATION_REQUIRED"])
    def updateQid(self, qid):
        """
        Record the queue id of the process.

        Args:
            qid: queue id of the process.

        Returns:
            None.

        Raises:
            Exception: if queue id is None or 0.

        """
        if not qid or qid == 0:
            raise Exception("Queue id cannot be None or 0.")

        self.logger.debug("Updating process {} with qid = {}.".format(
            self.processRecord.name, self.executionRecord.qid))
        self.executionRecord.qid = qid
        self.executionRecord.updateDatabase()

    @transactional(["PROPAGATION_REQUIRED"])
    def started(self, pid):
        """
        Mark the process as started and record its pid.

        Args:
            pid: process id.

        Returns:
            None.

        Raises:
            Exception if execution record does not exist or it's status is not 'Scheduled'.
        """
        if not hasattr(self, 'executionRecord'):
            raise Exception('Execution record should exist, was it scheduled?')

        if self.executionRecord.status != 'Scheduled':
            print self.executionRecord.status
            raise Exception("Execution record status should be None or 'Scheduled'.")

        self.executionRecord.pid = pid
        self.executionRecord.status = "Started"
        self.logger.debug("Marking process {} as 'Started' in database."
                          .format(self.processRecord.name))
        self.executionRecord.updateDatabase()

    @transactional(["PROPAGATION_REQUIRED"])
    def finished(self, returnValue):
        """
        Mark the process as finished and record its return value.

        Args:
            returnValue: return value from the process.

        Returns:
            None.

        Raises:
            Exception if process status does not exist or is not 'Started'.
        """
        if not self.executionRecord.status:
            raise Exception("Process status should not be None.")

        if self.executionRecord.status != "Started":
            raise Exception("Process status is not 'Started'.")

        self.executionRecord.status = "Finished"
        self.executionRecord.returnValue = returnValue
        self.logger.debug("Marking process {} as 'Finished' in database."
                          .format(self.processRecord.name))
        self.executionRecord.updateDatabase()

    @transactional(["PROPAGATION_REQUIRED"])
    def killed(self, returnValue=-9):
        """
        Marks the process as killed.

        Args:
            None.

        Returns:
            None.

        Raises:
            Exception: if process status does not exist or is not 'Started'.
        """
        if not self.executionRecord.status:
            raise Exception("Process status should not be None.")

        if self.executionRecord.status != "Started":
            raise Exception("Process status is not 'Started', rather, '%s'."
                            % (self.executionRecord.status,))

        self.executionRecord.status = "Killed"
        self.executionRecord.returnValue = returnValue
        self.logger.debug("Marking process {} as 'Killed' in database."
                          .format(self.processRecord.name))
        self.executionRecord.updateDatabase()

    @transactional(["PROPAGATION_REQUIRED"])
    def clean(self):
        """
        Delete all rows from Execution table. This is needed only for unit tests.

        Args:
            None.

        Returns:
            None.

        Raieses:
            None.
        """
        self.logger.warn("Deleting all execution history.")
        self.databaseTemplate.update(
            "DELETE FROM Execution WHERE process = %s", (self.processRecord.id,))
